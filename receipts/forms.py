from django.forms import ModelForm
from receipts.models import Receipt, ExpenseCategory, Account


class ReceiptCreate(ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]


class CategoryCreate(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name",
        ]


class AccountCreate(ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",
        ]
